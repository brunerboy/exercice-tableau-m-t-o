package exostableaumeteo;



import static exostableaumeteo.ExoTableauMeteoQ4.tableTemperatures;

public class ExoTableauMeteoQ4 {

    
    //<editor-fold defaultstate="collapsed" desc="Tableau des Températures">
    static int [][] tableTemperatures=
    {
            { 3, 4, 7, 5, -2, -3, 0, -4, -8, -8, -6, -3, 0, 4, 3,
              2, 5, 4, 5, 6, 6, 5, 6, 6, 8, 10, 10, 9, 8, 10, 12
            },
            { 12, 12, 11, 12, 10, 8,  8, 10, 10, 12, 12, 10,  9,  6, 6,
               5,  7,  7,  6,  8, 8, 10, 13, 14, 13, 15, 11, 10, 10 
            },
            {  9,  9,  8, 10, 10, 10,  9,  9,  8,  7,  8,  9, 11, 11, 13,
              13, 12, 11, 13, 15, 15, 14, 13, 13, 12, 12, 13, 15, 15, 13,13 
            },
            { 13, 14, 11, 10, 10,  8,  8, 10, 10, 11, 12, 13, 13, 14, 15,
              15, 13, 14, 15, 16, 16, 15, 13, 14, 14, 14, 15, 15, 16, 16 
            },
            { 16 ,16 ,15 ,15 ,16 ,16 ,15 ,14 ,14 ,13 ,14 ,14 ,15 ,14 ,13 ,
              17 ,17 ,16 ,17 ,17 ,18 ,18 ,19 ,20 ,20 ,21 ,19 ,19 ,18 ,18 ,18 
            },
            { 17, 17, 16, 16, 15, 16, 17, 17, 17, 18, 18, 19, 20, 21, 21,
              20, 21, 24, 23, 22, 21, 25, 25, 26, 26, 25, 25, 25, 25, 26 
            },
            { 28, 28, 27, 25, 25, 30, 30, 31, 30, 30, 29, 29, 25, 25, 24,
              24, 26, 28, 28, 28, 30, 28, 30, 31, 30, 29, 30, 31, 32, 33, 33 
            },
            { 33, 31, 33, 25, 26, 26, 28, 28, 29, 31, 33, 28, 24, 21,
              20, 23, 24, 23, 25, 25, 22, 22, 22, 21, 21, 21, 23, 23, 22, 23 
            },
            { 21, 22, 20, 20, 21, 21, 22, 22, 20, 20, 19, 20, 19, 19,19,
              18, 18, 19, 20, 20, 21, 19, 19, 18, 18, 20, 20, 21, 20, 19 
            },
            { 18, 18, 16, 15, 14, 14, 13, 14, 14, 15, 13, 13, 13, 12, 12,
              13, 13, 14, 14, 12, 12, 12, 13, 14, 14, 13, 13, 14, 13, 11, 10 
            },
            { 10, 10,  8, 5, 8, 8, 10, 10, 10, 13, 13, 10, 12, 12, 8,
              10, 10, 10, 8, 8, 8,  7,  7,  9,  6,  5,  7,  6,  3, 3 
            },
            {  1, 3, 2, 4, 2, 0, 2, 1, 0, 1, 2, 2, 1, 1, 0,
               0, -3, -4, -5, -5, -6, -6, -8, -8, -9, -9, -8, -8, -7, -6, -2 
            } 
       };
       
    
    
    //</editor-fold>
    
    
    static String[] tableMois={ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
                                "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"
                              };
    
    public static void main(String[] args) {
    
        //<editor-fold desc="Q4 Afficher les jours de l'année où il a gelé">
        System.out.println("Il a gelé aux dates suivantes : ");
       for (int i=0; i<tableTemperatures.length; i++) {
            for (int j=0; j<tableTemperatures[i].length; j++) {
                if (tableTemperatures[i][j] <= 0) {
                    System.out.println((j+1)+ " "+tableMois[i] +" "+tableTemperatures[i][j]+ "°");
                }
            }
       
        //</editor-fold>
    }
}
}









